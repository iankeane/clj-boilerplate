# Clj-boilerplate

This will be a boilerplate docker container for me to use to base future
clojure projects on.

It will just install deps, create a volume to develop on, forward the lein
repl port, and start lein. This workflow is intended to be used with
vim-fireplace

# TODO
* Change port assignment to be dynamic

# Usage
* sudo dockerd 
* docker-compose up
* open a clojure file in the clj1 dir or make sure to set filetype=clojure
* FireplaceConnect 0.0.0.0:7889 or Connect nrepl://0.0.0.0:7889
* Scope project to clj1 folder
